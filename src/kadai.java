import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class kadai {
    private JPanel root;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton checkOutButton;
    private JTextPane textPane1;
    private JLabel Label;

    static int sum;

    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "You have ordered " + food + " Thank you!");
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food + " " + price + "yen\n");
            sum += price;
            Label.setText("Total  " +sum+ "yen");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("kadai");
        frame.setContentPane(new kadai().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public kadai() {
        Label.setText("Total  " + sum + "yen");;
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Doria", 300);
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pizza", 400);
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Steak", 1000);
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Salad", 350);
            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Potato", 250);
            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soup", 150);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you! The total price  is " + sum + " yen.");
                    sum = 0;
                    textPane1.setText("");
                    Label.setText("Total  " + sum + "yen");
                }
            }
        });
    }
}
